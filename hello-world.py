from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello world in flask :) - new feature'

app.run(debug=True, host='0.0.0.0')  