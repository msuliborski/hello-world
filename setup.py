from setuptools import setup

setup(name='hello-world',
      version='0.9',
      description='Hello world',
      url='https://gitlab.com/msuliborski/hello-world',
      author='msuliborski',
      author_email='author_email@example.com',
      license='MIT',
      zip_safe=False)
